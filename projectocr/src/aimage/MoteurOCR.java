package aimage;


import ij.ImagePlus;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mcormelier on 24/03/17.
 */

public class MoteurOCR {

    private ArrayList<OCRImage> listeImg;

    public void createListeImage(String path) {
        File[] files = null;
        File directoryToScan = new File(path);
        files = directoryToScan.listFiles();

        if (files.length != 0) {
            for (int i = 0; i < files.length; i++) {
                ImagePlus tempImg = new ImagePlus(files[i].getAbsolutePath());
                new ImageConverter(tempImg).convertToGray8();
                listeImg.add(new OCRImage(tempImg,
                        files[i].getName().charAt(0),
                        files[i].getAbsolutePath()));
            }
        }
    }

    private Integer[][] confuseMatrix() {
        Integer[][] Matrix = new Integer[10][10];
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                Matrix[i][j] = 0;
        for (OCRImage image : listeImg) {
            int i = Character.getNumericValue(image.getLabel());
            int j = Character.getNumericValue(image.getDecision());
            Matrix[i][j]++;
        }
        return Matrix;
    }

    private ArrayList<String> MatrixToString(Integer[][] Matrix) {
        ArrayList<String> stringTab = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            String tmp = "";
            for (int j = 0; j < 10; j++)
                tmp += Matrix[i][j] + " | ";
            stringTab.add(tmp);
        }
        return stringTab;
    }


    // matrice de confusion (pour taux de reconnaissance,
    // diviser somme diagonale par 100)
    public void logOCR(String pathOut) {
        Integer[][] Matrix = confuseMatrix();
        List<String> lines = new ArrayList<>();
        lines.add("X | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |"); //Entête
        lines.addAll(MatrixToString(Matrix));
        int size = lines.size() - 1;
        for (int i = 0; i < size; i++) //Séparation
        {
            lines.add(2 * i + 1, "------------------------------------------");
        }
        float totalPercentages = 0.0f;
        for (int i = 2; i < lines.size(); i += 2) {
            int realValue = i / 2 - 1;
            int sum = 0;
            for (int j = 0; j < 10; j++)
                sum += Matrix[realValue][j];
            float percentage = ((float) Matrix[realValue][realValue]) / sum * 100;
            totalPercentages += percentage;
            lines.set(i, realValue + " | " + lines.get(i) + percentage + "%");
        }
        lines.add("");
        lines.add("Total: " + totalPercentages / 10 + "%");
        Path file = Paths.get(pathOut + "/pathOut.txt");
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void setFeaturesAll() {
        for (OCRImage img : listeImg) {
            img.setFeatures();
        }
    }

    private void setDecision(ArrayList<OCRImage> base) {
        int i = 0;
        for (OCRImage img : listeImg) {
            img.setDecision(base, i);
            i++;
        }
    }

    public MoteurOCR() {
        listeImg = new ArrayList<>();
    }

    private void resize(ImagePlus img, int larg, int haut) {
        ImageProcessor ip2 = img.getProcessor();
        ip2.setInterpolate(true);
        ip2 = ip2.resize(larg, haut);
        img.setProcessor(null, ip2);
    }

    public void resizeAll() {
        for (OCRImage img : listeImg)
            resize(img.getImg(), 20, 20);
    }


    public static void main(String[] args) {
        MoteurOCR moteur = new MoteurOCR();
        moteur.createListeImage("C:\\Users\\Adrien\\Desktop\\Image\\baseProjetOCR");
        moteur.resizeAll();
        moteur.setFeaturesAll();
        moteur.setDecision(moteur.getListeImg());
        moteur.logOCR("C:\\Users\\Adrien\\Desktop\\Image");
    }

    public ArrayList<OCRImage> getListeImg() {
        return listeImg;
    }
}
