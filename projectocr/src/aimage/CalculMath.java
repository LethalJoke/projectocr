package aimage;

import java.util.ArrayList;

/**
 * Created by mcormelier on 31/03/17.
 */
public class CalculMath {


    public static int PPV(ArrayList<Double> vect,
                          ArrayList<ArrayList<Double>> tabVect, int except) {
        double distMin = Double.MAX_VALUE;
        int bestVect = -1;
        for (int i = 0; i < tabVect.size(); i++) {
            if (i != except) {
                double dist = 0.0;
                for (int j = 0; j < tabVect.get(i).size(); j++)
                    dist += Math.pow(tabVect.get(i).get(j) - vect.get(j), 2);
                dist = Math.sqrt(dist);
                if (dist < distMin) {
                    distMin = dist;
                    bestVect = i;
                }
            }
        }
        return bestVect;
    }
}
