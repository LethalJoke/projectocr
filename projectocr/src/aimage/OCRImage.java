package aimage;

import ij.*;
import ij.process.ImageProcessor;

import java.util.ArrayList;

// utiliser zoning (diviser par 9)

/**
 * Created by mcormelier on 24/03/17.
 */
public class OCRImage {

    public ImagePlus getImg() {
        return img;
    }

    private ImagePlus img;
    private char label;
    private String path;
    private char decision;
    private ArrayList<Double> vect;

    static final int SEUIL = 127;

    public ArrayList<Double> getVect() {
        return vect;
    }


    public OCRImage(ImagePlus img, char label, String path) {
        this.img = img;
        this.label = label;
        this.path = path;
        vect = new ArrayList<>();
    }


    // Retourne la moyenne des niveaux de gris de l'image
    public double averageNdg(ImageProcessor ip) {
        byte[] pixelTab = (byte[]) ip.getPixels();
        int hauteur = ip.getHeight();
        int largeur = ip.getWidth();
        int sum = 0;
        for (int i = 0; i < hauteur; i++)
            for (int j = 0; j < largeur; j++)
                sum += (pixelTab[i * largeur + j]&0xff);
        return sum / pixelTab.length;
    }

    public void setDecision(ArrayList<OCRImage> base, int except) {
        ArrayList<ArrayList<Double>> featuresNdgs = new ArrayList<>();
        for (OCRImage img : base)
            featuresNdgs.add(img.getVect());
        int best = CalculMath.PPV(this.vect, featuresNdgs, except);
        decision = base.get(best).getLabel();
    }

    private void setFeatureNdg() {
        vect.add(averageNdg(img.getProcessor()));
    }

    private ArrayList<Double> getFeatureProfileH()
    {
        byte[] pixelTab = (byte[]) img.getProcessor().getPixels();
        ArrayList<Double> profileH = new ArrayList<>();
        int largeur = img.getProcessor().getWidth();
        int hauteur = img.getProcessor().getHeight();

        for (int i = 0; i < hauteur; i++) {
            int sum = 0;
            for (int j = 0; j < largeur; j++)
                if((pixelTab[i * largeur + j]&0xff) < SEUIL)
                    sum++;
            profileH.add(new Double(sum));
        }
        return profileH;
    }

    private ArrayList<Double> getFeatureProfileV()
    {
        byte[] pixelTab = (byte[]) img.getProcessor().getPixels();
        ArrayList<Double> profileV = new ArrayList<>();
        int largeur = img.getProcessor().getWidth();
        int hauteur = img.getProcessor().getHeight();

        for (int i = 0; i < largeur; i++) {
            int sum = 0;
            for (int j = 0; j < hauteur; j++)
                if((pixelTab[i * hauteur + j]&0xff)< SEUIL)
                sum++;
            profileV.add(new Double(sum));
        }
        return profileV;
    }

    private void setFeatureProfileHV()
    {
        vect.addAll(getFeatureProfileH());
        vect.addAll(getFeatureProfileV());
    }

    private void setRapportIso(){
        int rapport = 0;
        int perimeter = 0;
        byte[] pixelTab = (byte[]) img.getProcessor().getPixels();
        int largeur = img.getProcessor().getWidth();
        for(int i = 0; i< 400; i++) {
            if ((pixelTab[i]&0xff) < SEUIL) {
                rapport++;
                if (i%largeur != 0 && (pixelTab[i - 1]&0xff) > SEUIL )
                    perimeter++;
                else if (i%largeur!=19 && (pixelTab[i + 1]&0xff) > SEUIL)
                    perimeter++;
                else if(i>=largeur && (pixelTab[i-largeur]&0xff) > SEUIL)
                    perimeter++;
                else if(i<400-largeur && (pixelTab[i+largeur]&0xff) > SEUIL)
                    perimeter++;
            }
        }
        vect.add(((double)rapport)/perimeter);
    }

    private void setZoning(){ // division de l'image en 16 zones
        byte[] pixelTab = (byte[]) img.getProcessor().getPixels();
        ArrayList<Double> zones = new ArrayList<>();
        for(int i =0; i < 16; i++)
            zones.add(0.0);
        for(int i = 0; i < 400; i++)
        {
            int index = (i/100)*4+(i/25)%4;
            zones.set(index, zones.get(index)+(pixelTab[i]&0xff));
        }
        for(int i =0; i < 16; i++)
            zones.set(i, zones.get(i)/25);
        vect.addAll(zones);
    }


    public void setFeatures(){
        setFeatureNdg();
        setFeatureProfileHV();
        setRapportIso();
        setZoning();
        System.out.println("---");
        for(Double d : vect)
            System.out.println(d);
    }

    public char getLabel() {
        return label;
    }


    public char getDecision() {
        return decision;
    }

}
